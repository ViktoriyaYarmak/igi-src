package by.gsu.igi.students.YarmakViktoriya.laba3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] data;
        System.out.println("Текстовые данные: ");
        String inputData = scanner.nextLine();
        data = inputData.split(" ");
        System.out.println("Ожидаемые данные: ");
        System.out.println(data[0] + " " + data[1] + " = " + converter(data[0], data[1]) + " ms");
    }

    public static float converter(String value, String unit) {
        float value_in_ms = 0;
        if (unit.equals("ms")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 1;
        }
        if (unit.equals("kmh")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 0.278;
        }
        if (unit.equals("mph")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 0.447;
        }
        if (unit.equals("kn")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 0.514;
        }
        return value_in_ms;
    }
}
