package by.gsu.igi.students.YarmakViktoriya.lab6;

public class Marathon {
    public static void main(String[] args) {
        String[] names = {
                "Елена", "Дмитрий", "Сергей", "Андрей",
                "Ольга", "Виктория", "Юлия", "Эмма",
                "Алексей", "Вероника", "Анна", "Вадим",
                "Тимофей", "Иван", "Екатерина", "Павел"
        };

        int[] times = {
                341, 273, 278, 329, 445, 402, 388, 275,
                243, 334, 412, 393, 299, 343, 317, 265
        };

        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i] + ":" + times[i]);
        }
        int idMin1Time = idMin1Time(times);
        int idMin2Time = idMin2Time(times, idMin1Time);
        System.out.println("\nСамый быстрый бегун\n" + names[idMin1Time] + ":" + times[idMin1Time]);
        System.out.println("\nВторой быстрый бегун\n" + names[idMin2Time] + ":" + times[idMin2Time]);
    }

    public static int idMin1Time(int[] massive) {
        int min = massive[0];
        int idMin = -1;
        for (int i = 0; i < massive.length; i++) {
            if (massive[i] < min) {
                idMin = i;
                min = massive[i];
            }
        }
        return idMin;
    }

    public static int idMin2Time(int[] massive, int idMin1Time) {
        int min = massive[0];
        int idMin = -1;
        for (int i = 0; i < massive.length; i++) {
            if (massive[i] < min && massive[i] > massive[idMin1Time]) {
                idMin = i;
                min = massive[i];
            }
        }
        return idMin;
    }
}
