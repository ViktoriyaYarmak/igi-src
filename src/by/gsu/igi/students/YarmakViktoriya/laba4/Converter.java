package by.gsu.igi.students.YarmakViktoriya.laba4;

public class Converter {

    public static float converter(String value, String unit) {
        float value_in_ms = 0;
        if (unit.equals("ms")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 1;
        }
        if (unit.equals("kmh")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 0.278;
        }
        if (unit.equals("mph")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 0.447;
        }
        if (unit.equals("kn")) {
            value_in_ms = Float.parseFloat(value);
            value_in_ms *= 0.514;
        }
        return value_in_ms;
    }

}
