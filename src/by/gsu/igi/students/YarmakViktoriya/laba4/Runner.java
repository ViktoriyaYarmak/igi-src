package by.gsu.igi.students.YarmakViktoriya.laba4;

public class Runner {

    public static void main(String[] args) {
        System.out.println("Текстовые данные: ");
        Reader inputData = new Reader();
        String[] mas = inputData.massive();
        System.out.println("Ожидаемые данные: ");
        System.out.println(mas[0] + mas[1] + " = " + Converter.converter(mas[0], mas[1]) + " ms");
    }

}
